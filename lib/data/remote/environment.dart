enum Environments { PRODUCTION, DEV }

class ConfigEnvironments {
  static String _currentEnvironment = Environments.DEV.name;

  static setEnvironment(Environments value) => _currentEnvironment = value.name;

  static final List<Map<String, String>> _availableEnvironments = [
    {
      'env': Environments.DEV.name,
      'url': 'http://panti.suitdev.com/',
    },
    {
      'env': Environments.PRODUCTION.name,
      'url': 'http://panti.suitdev.com/',
    },
  ];

  static String? getEnvironment() {
    return _currentEnvironment;
  }

  static String? getBaseUrlAPI() {
    return _availableEnvironments.firstWhere(
      (d) => d['env'] == _currentEnvironment,
    )['url'];
  }
}

import 'dart:convert';
import 'dart:developer';

import 'package:hive_flutter/hive_flutter.dart';
import 'package:suitcore_flutter_base_code_provider/data/model/converter.dart';
import 'package:suitcore_flutter_base_code_provider/model/storage.dart';
import 'package:suitcore_flutter_base_code_provider/utills/helper/map_helper.dart';

mixin CacheMixin<T> {
  String cachedTag = 'CacheMixin::->';

  final _box = Hive.box<Storage>((Storage).toString());

  Future<List<T>> getCacheList({required String cacheKey}) async {
    log("get storage key : $cacheKey");
    var cache = _box.get(cacheKey);
    if (cache != null && cache.toString().isNotEmpty) {
      return List<T>.from(
        json.decode(cache.value).map(
              (x) => Converter<T>().fromJson(x),
            ),
      );
    } else {
      return [];
    }
  }

  Future<T?> getCacheObject({required String cacheKey, String id = '0'}) async {
    log("get storage key : $cacheKey");
    var cache = _box.get(cacheKey);
    if (cache != null && cache.toString().isNotEmpty) {
      var data = convertDynamicMap(cache.value);
      if (id == _getId(data)) {
        return Converter<T>().fromJson(data);
      }
    }
    return null;
  }

  String _getId(Map<String, dynamic> cache) {
    return (cache['id'] ?? '0').toString();
  }

  Future<void> saveCache({
    required String cacheKey,
    List<T>? list,
    T? data,
    int page = 1,
  }) async {
    _box.put(
      cacheKey,
      Storage(
        key: cacheKey,
        value: list != null
            ? json.encode(list)
            : (data != null ? Converter<T>().toJson(data) : ""),
      ),
    );
  }
}

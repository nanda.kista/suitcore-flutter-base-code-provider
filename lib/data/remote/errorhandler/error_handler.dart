import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/errorhandler/network_exception.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/wrapper/api_response.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/wrapper/base_response.dart';
import 'package:suitcore_flutter_base_code_provider/feature/auth/auth_controller.dart';

class ErrorHandler {
  static NetworkExceptions getDioException(error) {
    debugPrint("getDioException: ${error.toString()}");

    if (error is Exception) {
      NetworkExceptions networkExceptions;
      if (error is DioException) {
        switch (error.type) {
          case DioExceptionType.connectionTimeout:
            networkExceptions = const NetworkExceptions.requestTimeout();
            break;
          case DioExceptionType.sendTimeout:
            networkExceptions = const NetworkExceptions.sendTimeout();
            break;
          case DioExceptionType.receiveTimeout:
            networkExceptions = const NetworkExceptions.receiveTimeout();
            break;
          case DioExceptionType.cancel:
            networkExceptions = const NetworkExceptions.requestCancelled();
            break;
          case DioExceptionType.badResponse:
            switch (error.response?.statusCode) {
              case 400:
                networkExceptions = const NetworkExceptions.badRequest();
                break;
              case 401:
                networkExceptions =
                    const NetworkExceptions.unauthorisedRequest();
                break;
              case 403:
                networkExceptions =
                    const NetworkExceptions.unauthorisedRequest();
                break;
              case 404:
                networkExceptions =
                    const NetworkExceptions.notFound("Not found");
                break;
              case 405:
                networkExceptions = const NetworkExceptions.methodNotAllowed();
                break;
              case 406:
                networkExceptions = const NetworkExceptions.notAcceptable();
                break;
              case 409:
                networkExceptions = const NetworkExceptions.conflict();
                break;
              case 408:
                networkExceptions = const NetworkExceptions.requestTimeout();
                break;
              case 500:
                networkExceptions =
                    const NetworkExceptions.internalServerError();
                break;
              case 503:
                networkExceptions =
                    const NetworkExceptions.serviceUnavailable();
                break;
              default:
                final responseCode = error.response?.statusCode;
                final message = error.response?.statusMessage;
                networkExceptions = NetworkExceptions.defaultError(
                  "Received invalid status code: $responseCode $message",
                );
            }

            if (error.response != null &&
                networkExceptions !=
                    const NetworkExceptions.unauthorisedRequest()) {
              networkExceptions =
                  networkExceptions.tryExtractFromResponse(error.response!);
            }
            break;
          default:
            networkExceptions = NetworkExceptions.defaultError(
                error.message?.toString() ?? error.error.toString());
            break;
        }
      } else if (error is FormatException) {
        networkExceptions = const NetworkExceptions.formatException();
      } else if (error is SocketException) {
        networkExceptions = const NetworkExceptions.noInternetConnection();
      } else {
        networkExceptions = const NetworkExceptions.unexpectedError();
      }
      return networkExceptions;
    } else if (error.toString().contains("is not a subtype of")) {
      return const NetworkExceptions.unableToProcess();
    } else {
      return NetworkExceptions.defaultError(error.toString());
    }
  }

  static String getErrorMessage(NetworkExceptions networkExceptions) {
    var errorMessage = "";
    networkExceptions.when(notImplemented: () {
      errorMessage = "Not implemented";
    }, requestCancelled: () {
      errorMessage = "Request cancelled";
    }, internalServerError: () {
      errorMessage = "Internal server error";
    }, notFound: (String reason) {
      errorMessage = reason;
    }, serviceUnavailable: () {
      errorMessage = "Service unavailable";
    }, methodNotAllowed: () {
      errorMessage = "Method not allowed";
    }, badRequest: () {
      errorMessage = "Bad request";
    }, unauthorisedRequest: () {
      errorMessage = "Unauthorised request";
    }, unexpectedError: () {
      errorMessage = "Unexpected error occurred";
    }, requestTimeout: () {
      errorMessage = "Connection request timeout";
    }, noInternetConnection: () {
      errorMessage = "No internet connection";
    }, conflict: () {
      errorMessage = "Error due to a conflict";
    }, sendTimeout: () {
      errorMessage = "Send timeout in connection with API server";
    }, receiveTimeout: () {
      errorMessage = "Receive timeout in connection with API server";
    }, unableToProcess: () {
      errorMessage = "Unable to process the data";
    }, defaultError: (String error) {
      errorMessage = error;
    }, formatException: () {
      errorMessage = "Invalid format";
    }, notAcceptable: () {
      errorMessage = "Not acceptable";
    });
    return errorMessage;
  }
}

extension FutureAPIResultExt<T extends BaseResponse> on Future<T> {
  Future<T> validateStatus() {
    return then((value) async {
      final code = value.status;
      final message = value.message;
      // You can use this if has multiple domain url and separate the error
      // final domain = value.domain;
      // debugPrint("DOMAIN => $domain");
      if (code >= 200 && code <= 299) {
        return value;
      }

      if ([401, 403].contains(code)) {
        AuthController.find.signOut();
      }

      throw message;
    });
  }

  Future<T> validateData() {
    return then((value) {
      final error = ErrorHandler.getErrorMessage(
          const NetworkExceptions.formatException());
      if (value is ResponseObject && value.data == null) {
        throw error;
      }
      if (value is ResponseList && value.data == null) {
        throw error;
      }
      return value;
    });
  }

  Future<T> validateResponse() {
    return validateStatus().validateData();
  }
}

extension FutureExt<T> on Future<T> {
  Future<T> safeError() {
    return catchError((error) async {
      final exception = ErrorHandler.getDioException(error);

      if (exception == const NetworkExceptions.unauthorisedRequest()) {
        AuthController.find.signOut();
      }

      throw ErrorHandler.getErrorMessage(exception);
    });
  }
}

// MARK: - HELPER

extension NetworkExceptionsExt on NetworkExceptions {
  /// try to extract error message and replace it using value from body, else default.
  ///
  /// ini digunakan untuk kasus saat http header status codenya bukan 2xx,
  /// meanwhile pesan error aslinya ada di http body-nya.
  /// maka perlu coba kita ekstrak.
  ///
  NetworkExceptions tryExtractFromResponse(Response<dynamic> response) {
    final data = response.data;

    Map<String, dynamic> json = {};

    // cek hanya jika response body-nya berupa json.
    //
    if (data is Map<String, dynamic>) {
      json = data;
    }

    // coba mengambil pesan error dari field message dll.
    //
    final message = json["message"]?.toString().trim() ??
        json["info"]?.toString().trim() ??
        json["note"]?.toString().trim() ??
        "";
    if (message.isNotEmpty) {
      final code = response.statusCode;
      final fullMessage = "($code) $message";
      return NetworkExceptions.defaultError(fullMessage);
    }

    return this;
  }
}

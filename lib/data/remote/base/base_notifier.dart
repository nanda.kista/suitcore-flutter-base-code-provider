import 'dart:async';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh_flutter3/pull_to_refresh_flutter3.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/base_refresher_status.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/mixin/auto_retry_mixin.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/mixin/cache_mixin.dart';
import 'dart:developer';

abstract class BaseNotifier<T> extends ChangeNotifier
    with AutoRetryMixin, CacheMixin<T> {
  T? dataObj;
  List<T> dataList = [];

  int page = 1;
  bool hasNext = false;
  int perPage = 10;

  String message = "";

  get statusData;
  bool get isUsingList => statusData is List;
  String get cacheKey => isUsingList ? "${T.toString()}_list" : T.toString();
  final cancelToken = CancelToken();

  RefresherStatus status = RefresherStatus.initial;

  bool get isInitial => status == RefresherStatus.initial;

  /// **Note:**
  /// loading with probably data is not empty
  bool get isLoading => status == RefresherStatus.loading;

  /// **Note:**
  /// loading with no data from the beginning
  bool get isShimmering => isLoading && isEmptyData;
  bool get isEmptyData => isUsingList ? dataList.isEmpty : dataObj == null;
  bool get isSuccess => status == RefresherStatus.success;
  bool get isError => status == RefresherStatus.failed && isEmptyData;

  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  @mustCallSuper
  void onInit([dynamic args]) {
    initAutoRetry(
      onRefresh: () {
        if (isError && !isLoading) refreshPage();
      },
    );
  }

  void refreshPage([BuildContext? context]);

  void loadNextPage([BuildContext? context]) {}

  @override
  @mustCallSuper
  void dispose() {
    onClose();
    disposeAutoRetry();
    cancelToken.cancel();
    super.dispose();
  }

  @mustCallSuper
  void onClose() {}

  /// **NOTE:**
  /// make sure you call this method at initial state, before you call method [saveCacheAndFinish]
  Future<void> getCache({String id = '0'}) async {
    log("get storage key : $cacheKey");
    if (isUsingList) {
      _setFinishCallbacks(list: await getCacheList(cacheKey: cacheKey));
    } else {
      _setFinishCallbacks(
          data: await getCacheObject(cacheKey: cacheKey, id: id));
    }
  }

  /// **NOTE:**
  /// call this to finish the load data,
  /// don't need to call [finishLoadData] anymore
  Future<void> saveCacheAndFinish({
    List<T>? list,
    T? data,
    int page = 1,
  }) async {
    saveCache(cacheKey: cacheKey, list: list, data: data, page: page);
    finishLoadData(list: list, data: data, page: page);
  }

  /// **Note:**
  /// the state will go to error state if the [errorMessage] is not null,
  /// call this [finishLoadData] instead [saveCacheAndFinish] if the data is not require to saved in local data
  void finishLoadData({
    String errorMessage = "",
    List<T>? list = const [],
    T? data,
    int page = 1,
  }) {
    this.page = page;
    if (errorMessage.isNotEmpty) {
      _setErrorStatus(errorMessage);
    } else {
      _setFinishCallbacks(list: list, data: data);
    }
    notifyListeners();
  }

  /// **NOTE:**
  /// call this to change state to Loading State
  void loadingState() {
    status = RefresherStatus.loading;
    notifyListeners();
  }

  /// **NOTE:**
  /// call this to change state to Success State
  void successState() {
    status = RefresherStatus.success;
    notifyListeners();
  }

  /// **NOTE:**
  /// call this to change state to Success State
  void emptyState() {
    status = RefresherStatus.empty;
    notifyListeners();
  }

  /// **NOTE:**
  /// call this to change state to Error State
  void errorState() {
    status = RefresherStatus.failed;
    notifyListeners();
  }

  void _addData(List<T> data) {
    if (page == 1) {
      dataList.clear();
    }
    if (data.isNotEmpty) {
      dataList.addAll(data);
    } else {
      if (isUsingList) {
        emptyState();
      }
    }
    hasNext = !(data.length < perPage);
  }

  void _setData(T? data) {
    if (data != null) {
      dataObj = data;
    } else if (!isUsingList) {
      emptyState();
    }
  }

  void _setFinishCallbacks({List<T>? list, T? data}) {
    _addData(list ?? []);
    _setData(data);
    successState();
    _finishRefresh();
  }

  void _setErrorStatus(String message) {
    errorState();
    this.message = (message.isNotEmpty) ? message : 'txt_error_title'.tr();
    _finishRefresh();
  }

  void _finishRefresh() {
    if (refreshController.isRefresh) {
      refreshController.refreshCompleted();
    }
    if (refreshController.isLoading) {
      refreshController.loadComplete();
    }
    notifyListeners();
  }
}

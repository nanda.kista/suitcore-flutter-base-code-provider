import 'package:provider/single_child_widget.dart';

abstract class Bindings {
  List<SingleChildWidget> dependencies(dynamic args);
}

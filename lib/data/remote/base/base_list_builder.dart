import 'package:flutter/cupertino.dart';
import 'package:pull_to_refresh_flutter3/pull_to_refresh_flutter3.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/base_notifier.dart';

class BaseListBuilder<T> extends StatelessWidget {
  final BaseNotifier<T> notifier;
  final Widget Function(BuildContext context, int index) item;

  const BaseListBuilder(
    this.notifier, {
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      enablePullDown: true,
      enablePullUp: notifier.hasNext,
      controller: notifier.refreshController,
      onRefresh: notifier.refreshPage,
      onLoading: notifier.loadNextPage,
      child: ListView.builder(
        itemCount: notifier.dataList.length,
        itemBuilder: (context, index) => item(context, index),
      ),
    );
  }
}

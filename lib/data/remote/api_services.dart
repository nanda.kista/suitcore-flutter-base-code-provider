import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:suitcore_flutter_base_code_provider/model/login_result.dart';
import 'package:suitcore_flutter_base_code_provider/model/place.dart';
import 'package:suitcore_flutter_base_code_provider/data/local/secure/secure_storage_manager.dart';
import 'environment.dart';
import 'interceptor/dio.dart';
import 'wrapper/api_response.dart';

part 'api_services.g.dart';

@RestApi()
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  static Future<RestClient> create({
    Map<String, dynamic> headers = const {},
    int connectTimeout = 30000,
    int receiveTimeout = 30000,
  }) async {
    final Map<String, dynamic> newHeaders = {
      'Accept': 'application/json',
    };

    newHeaders.addAll(headers);

    final token = await SecureStorageManager().getToken() ?? "";
    if (!newHeaders.containsKey("Authorization") && token.isNotEmpty) {
      newHeaders["Authorization"] = token;
    }

    // newHeaders["Locale"] = LocaleHelper().getCurrentLocale().languageCode;

    return RestClient(
      await AppDio().getDIO(
        headers: newHeaders,
        connectTimeout: connectTimeout,
        receiveTimeout: receiveTimeout,
      ),
      baseUrl: ConfigEnvironments.getBaseUrlAPI().toString(),
    );
  }

  @POST("/api/user/login")
  Future<ResponseObject<LoginResult>> login(
      @CancelRequest() CancelToken cancelToken,
      @Query("email") String email,
      @Query("password") String password);

  @GET("/api/places")
  Future<ResponseList<Place>> getPlaces(
      @CancelRequest() CancelToken cancelToken,
      @Query("page") int page,
      @Query("perPage") int perPage);

  @GET("/api/places/{id}")
  Future<ResponseObject<Place>> getPlaceDetail(
    @CancelRequest() CancelToken cancelToken,
    @Path("id") int id,
  );
}

const createClient = RestClient.create;

import 'package:dio/dio.dart';
import 'log_interceptor.dart';

class AppDio {
  Future<Dio> getDIO({
    Map<String, dynamic> headers = const {},
    int connectTimeout = 30000,
    int receiveTimeout = 30000,
  }) async {
    final dio = Dio(BaseOptions(
      connectTimeout: Duration(milliseconds: connectTimeout),
      receiveTimeout: Duration(milliseconds: receiveTimeout),
      contentType: "application/json",
    ));

    // apply new headers
    dio.options.headers.addAll(headers);

    // dio.interceptors.add(LogInterceptor(responseBody: true));
    dio.interceptors.add(APILogInterceptor());

    return dio;
  }
}

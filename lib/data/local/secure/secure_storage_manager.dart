import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:suitcore_flutter_base_code_provider/initializer.dart';

class SecureStorageManager {
  static FlutterSecureStorage secureStorage = sl<FlutterSecureStorage>();

  final String _tokenKey = "token";

  // Token
  //
  Future<String?> getToken() async {
    return secureStorage.read(key: _tokenKey);
  }

  Future<void> setToken({String? value}) async {
    return secureStorage.write(key: _tokenKey, value: value);
  }
}

import 'package:hive_flutter/hive_flutter.dart';
import 'package:suitcore_flutter_base_code_provider/model/storage.dart';

class HiveHelper {
  var box = Hive.box<Storage>((Storage).toString());

  void save(String key, Storage data) {
    box.put(key, data);
  }

  void delete(String key) {
    box.delete(key);
  }

  Storage? read(String key) => box.get(key);
}

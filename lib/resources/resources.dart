import 'package:flutter/material.dart';
import 'package:suitcore_flutter_base_code_provider/resources/assets.gen.dart'
    as asset;

// ignore: non_constant_identifier_names
final AppImages = asset.Assets.lib.resources.images;

class AppColors {
  static const colorPrimary = Colors.deepOrange;
  static const colorSecondary = Colors.deepOrangeAccent;
  static const colorAccent = Colors.white;
  static const black = Colors.black;
  static const white = Colors.white;
  static const grey = Colors.grey;
  static const red = Colors.red;
  static const borderColor = Colors.black12;
  static const subHintColor = Colors.black45;
}

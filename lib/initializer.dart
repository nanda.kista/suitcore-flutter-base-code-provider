import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:suitcore_flutter_base_code_provider/utills/localization/locale_helper.dart';
import 'data/remote/base/main_navigation.dart';
import 'feature/auth/auth_controller.dart';
import 'model/storage.dart';

final sl = GetIt.instance;
class Initializer {
  static Future<void> init() async {
    try {
      // Fix isu CERTIFICATE_VERIFY_FAILED on Android 6 and below
      // - https://gitlab.com/suitmedia/suitcore-flutter-base-code-getx/-/issues/26
      // - https://stackoverflow.com/a/71090239/2537616
      ByteData data =
          await PlatformAssetBundle().load('assets/ca/lets-encrypt-r3.pem');
      SecurityContext.defaultContext
          .setTrustedCertificatesBytes(data.buffer.asUint8List());

      _initScreenPreference();
      await Initializer.initHive();

      sl.registerSingleton(const FlutterSecureStorage(
        aOptions: AndroidOptions(encryptedSharedPreferences: true),
      ));
      sl.registerLazySingleton(() => LocaleHelper());
      sl.registerLazySingleton(() => Navigation());
      sl.registerSingleton(AuthController());
    } catch (err) {
      rethrow;
    }
  }

  static Future<void> initHive() async {
    Directory dir = Directory("");
    if (Platform.isIOS) {
      dir = await getLibraryDirectory();
    } else {
      dir = await getApplicationDocumentsDirectory();
    }
    debugPrint("Hive Save to Location : ${dir.path}");
    Hive
      ..init(dir.path)
      ..registerAdapter(StorageAdapter());
    await Hive.openBox<Storage>((Storage).toString());
  }

  static Future<void> _initScreenPreference() {
    return SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }
}

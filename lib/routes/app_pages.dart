import 'package:go_router/go_router.dart';
import 'package:suitcore_flutter_base_code_provider/feature/dashboardtab/dashboard_tab_route.dart';
import 'package:suitcore_flutter_base_code_provider/feature/loader/loading_page.dart';
import 'package:suitcore_flutter_base_code_provider/feature/loader/loading_route.dart';
import 'package:suitcore_flutter_base_code_provider/feature/login/login_route.dart';
import 'package:suitcore_flutter_base_code_provider/feature/other/other_route.dart';
import 'package:suitcore_flutter_base_code_provider/feature/places/places_route.dart';
import 'package:suitcore_flutter_base_code_provider/feature/places/place_detail/place_detail_route.dart';

import 'route_observer.dart';
import 'route_error_page.dart';

abstract class AppRoutes {
  static GoRouter router = GoRouter(
    initialLocation: LoadingPage.route,
    debugLogDiagnostics: true,
    routerNeglect: true,
    observers: [
      RouteObserver(),
    ],
    errorBuilder: (context, state) => RouteErrorPage(state: state),
    routes: [
      ...loginRoute,
      ...loaderRoute,
      ...dashboardRoute,
      ...otherRoute,
      ...placesRoute,
      ...placeDetailRoute,
    ],
  );
}

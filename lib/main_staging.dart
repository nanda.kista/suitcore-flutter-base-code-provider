import './data/remote/environment.dart';
import './main.dart' as main_app;

void main() async {
  ConfigEnvironments.setEnvironment(Environments.DEV);
  main_app.main();
}

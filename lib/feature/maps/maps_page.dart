import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class MapsPage extends StatelessWidget {
  const MapsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('txt_menu_maps'.tr()),
      ),
      body: const Center(child: Text('Maps')),
    );
  }
}

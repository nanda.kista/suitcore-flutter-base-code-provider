import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/notifier_view.dart';
import 'package:suitcore_flutter_base_code_provider/feature/dialog/sample_dialog_page.dart';
import 'package:suitcore_flutter_base_code_provider/feature/maps/maps_page.dart';
import 'package:suitcore_flutter_base_code_provider/feature/other/other_page.dart';
import 'package:suitcore_flutter_base_code_provider/feature/places/places_page.dart';
import 'package:suitcore_flutter_base_code_provider/utills/widget/colored_status_bar.dart';

import 'dashboard_tab_notifier.dart';

class DashboardTabPage extends NotifierView<DashboardTabNotifier> {
  static const String route = '/dashboard';

  const DashboardTabPage({super.key});

  @override
  Widget build(BuildContext context, DashboardTabNotifier notifier) {
    return ColoredStatusBar(
      child: Scaffold(
        body: SafeArea(
          child: Selectable(
            selector: (notifier) => notifier.tabIndex,
            builder: (index) => IndexedStack(
              index: index,
              children: const [
                PlacesPage(),
                DialogPage(),
                MapsPage(),
                OtherPage(),
              ],
            ),
          ),
        ),
        bottomNavigationBar: Container(
          decoration: const BoxDecoration(
            color: Colors.grey,
            border: Border(
              top: BorderSide(color: Colors.white, width: 1.0),
            ),
          ),
          child: Selectable(
            selector: (notifier) => notifier.tabIndex,
            builder: (index) => BottomNavigationBar(
              onTap: notifier.changeTabIndex,
              currentIndex: notifier.tabIndex,
              type: BottomNavigationBarType.fixed,
              showSelectedLabels: true,
              showUnselectedLabels: true,
              unselectedItemColor: Colors.grey,
              items: [
                _bottomNavigationBarItem(
                  icon: CupertinoIcons.home,
                  label: 'txt_menu_home'.tr(),
                ),
                _bottomNavigationBarItem(
                  icon: CupertinoIcons.book,
                  label: 'txt_menu_dialog'.tr(),
                ),
                _bottomNavigationBarItem(
                  icon: CupertinoIcons.map,
                  label: 'txt_menu_maps'.tr(),
                ),
                _bottomNavigationBarItem(
                  icon: CupertinoIcons.bag,
                  label: 'txt_menu_other'.tr(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  BottomNavigationBarItem _bottomNavigationBarItem(
      {required IconData icon, required String label}) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      label: label,
    );
  }
}

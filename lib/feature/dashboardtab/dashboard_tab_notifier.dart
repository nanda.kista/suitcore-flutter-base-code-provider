import 'package:flutter/material.dart';

class DashboardTabNotifier extends ChangeNotifier {
  var tabIndex = 0;

  void changeTabIndex(int index) {
    tabIndex = index;
    notifyListeners();
  }
}

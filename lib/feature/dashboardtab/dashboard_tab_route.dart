import 'package:suitcore_flutter_base_code_provider/feature/dashboardtab/dashboard_tab_binding.dart';
import 'package:suitcore_flutter_base_code_provider/feature/dashboardtab/dashboard_tab_page.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/route_page.dart';

final dashboardRoute = [
  RoutePage(
    name: DashboardTabPage.route,
    page: (args) => const DashboardTabPage(),
    binding: DashboardTabBinding(),
  ),
];

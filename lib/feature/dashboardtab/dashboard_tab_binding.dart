import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/route_binding.dart';
import 'package:suitcore_flutter_base_code_provider/feature/dashboardtab/dashboard_tab_notifier.dart';
import 'package:suitcore_flutter_base_code_provider/feature/other/other_binding.dart';
import 'package:suitcore_flutter_base_code_provider/feature/places/places_binding.dart';

class DashboardTabBinding extends Bindings {
  @override
  List<SingleChildWidget> dependencies(args) {
    return [
      ...PlacesBinding().dependencies(args),
      ...OtherBinding().dependencies(args),
      ChangeNotifierProvider(create: (_) => DashboardTabNotifier()),
    ];
  }
}

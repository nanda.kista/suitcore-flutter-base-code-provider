import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/notifier_view.dart';
import 'package:suitcore_flutter_base_code_provider/feature/auth/auth_controller.dart';
import 'package:suitcore_flutter_base_code_provider/resources/resources.dart';
import 'package:suitcore_flutter_base_code_provider/utills/helper/validator.dart';
import 'package:suitcore_flutter_base_code_provider/utills/widget/primary_button.dart';

import 'login_notifier.dart';

class LoginPage extends NotifierView<LoginNotifier> {
  static const String route = '/login';
  final AuthController authController = AuthController.find;

  LoginPage({super.key});

  @override
  Widget build(BuildContext context, LoginNotifier notifier) {
    return Scaffold(
      body: SafeArea(
        top: true,
        bottom: true,
        child: FormBuilder(
          key: notifier.formKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Center(
              child: SingleChildScrollView(
                child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 24),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 30, vertical: 20),
                          child: AppImages.icLogoSuitcoreMain.image(
                            color: AppColors.colorPrimary,
                          ),
                        ),
                        const SizedBox(height: 48.0),
                        FormBuilderTextField(
                          name: 'email',
                          enabled: !notifier.isLoading,
                          decoration: InputDecoration(
                            hintText:
                                "${'txt_email'.tr()} (ex: rifat@suitmedia.com)",
                            prefixIcon: const Icon(Icons.mail),
                          ),
                          validator: Validator.required(),
                          keyboardType: TextInputType.emailAddress,
                        ),
                        const FormVerticalSpace(),
                        FormBuilderTextField(
                          name: 'password',
                          enabled: !notifier.isLoading,
                          decoration: InputDecoration(
                            hintText: "${'txt_password'.tr()} (ex: 12341234)",
                            prefixIcon: const Icon(Icons.lock),
                          ),
                          validator: Validator.required(),
                          obscureText: true,
                          maxLines: 1,
                        ),
                        const FormVerticalSpace(height: 48),
                        notifier.isLoading
                            ? const CircularProgressIndicator()
                            : Container(),
                        notifier.isLoading
                            ? Container()
                            : PrimaryButton(
                                text: 'txt_button_login'.tr(),
                                onPressed: () async {
                                  if (notifier.formKey.currentState != null &&
                                      notifier.formKey.currentState!
                                          .saveAndValidate()) {
                                    notifier
                                        .signInWithEmailAndPassword(context);
                                  }
                                }),
                        const FormVerticalSpace(height: 20),
                        notifier.isLoading
                            ? Container()
                            : PrimaryButton(
                                text: 'Skip',
                                reverse: true,
                                onPressed: () async {
                                  notifier.bypassLogin();
                                }),
                      ],
                    )),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class FormVerticalSpace extends SizedBox {
  const FormVerticalSpace({super.key, double height = 24.0})
      : super(height: height);
}

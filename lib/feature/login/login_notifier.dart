import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/api_services.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/base_notifier.dart';
import 'package:suitcore_flutter_base_code_provider/feature/auth/auth_controller.dart';
import 'package:suitcore_flutter_base_code_provider/model/user.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/errorhandler/error_handler.dart';

class LoginNotifier extends BaseNotifier<User> {
  final AuthController authController = AuthController.find;
  final GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();

  @override
  get statusData => dataObj;

  @override
  void refreshPage([BuildContext? context]) {}

  @override
  Future<void> onInit([dynamic args]) async {
    super.onInit();

    final permissionStorage = await Permission.storage.status;
    if (permissionStorage.isDenied) {
      await Permission.storage.request();
    }

    final permissionNotif = await Permission.notification.status;
    if (permissionNotif.isDenied) {
      await Permission.notification.request();
    }
  }

  Future<void> signInWithEmailAndPassword(BuildContext context) async {
    final email = formKey.currentState?.value['email'] as String;
    final password = formKey.currentState?.value['password'] as String;

    loadingState();

    try {
      final client = await createClient();

      final result = await client
          .login(cancelToken, email, password)
          .validateResponse()
          .safeError();

      finishLoadData(data: result.data!.user);

      await authController.saveAuthData(
        user: result.data!.user!,
        token: result.data!.token!,
      );
    } catch (error) {
      finishLoadData(errorMessage: error.toString());
    }
  }

  Future<void> bypassLogin() async {
    loadingState();
    await Future.delayed(const Duration(seconds: 2));
    var user = User(
        id: 1,
        name: "suitmedian",
        email: "@suitmedia.com",
        gender: "none",
        status: "admin");
    authController.saveAuthData(user: user, token: "a");
    finishLoadData();
    authController.setAuth();
  }
}

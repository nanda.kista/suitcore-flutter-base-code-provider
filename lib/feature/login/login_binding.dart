import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/route_binding.dart';
import 'package:suitcore_flutter_base_code_provider/feature/login/login_notifier.dart';

class LoginBinding extends Bindings {
  @override
  List<SingleChildWidget> dependencies(dynamic args) {
    return [
      ChangeNotifierProvider(
        create: (_) => LoginNotifier()..onInit(args),
      ),
    ];
  }
}

import 'package:suitcore_flutter_base_code_provider/feature/login/login_binding.dart';
import 'package:suitcore_flutter_base_code_provider/feature/login/login_page.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/route_page.dart';

final loginRoute = [
  RoutePage(
    name: LoginPage.route,
    page: (args) => LoginPage(),
    binding: LoginBinding(),
  ),
];

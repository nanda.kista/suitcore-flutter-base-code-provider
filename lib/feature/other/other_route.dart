import 'package:suitcore_flutter_base_code_provider/feature/other/other_binding.dart';
import 'package:suitcore_flutter_base_code_provider/feature/other/other_page.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/route_page.dart';

final otherRoute = [
  RoutePage(
    name: OtherPage.route,
    page: (args) => const OtherPage(),
    binding: OtherBinding(),
  ),
];

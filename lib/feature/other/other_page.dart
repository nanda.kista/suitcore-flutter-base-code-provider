import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/notifier_view.dart';
import 'package:suitcore_flutter_base_code_provider/utills/localization/locale_helper.dart';
import 'package:suitcore_flutter_base_code_provider/utills/widget/forms/form_image_picker.dart';

import 'other_notifier.dart';

class OtherPage extends NotifierView<OtherNotifier> {
  static const String route = '/other';

  const OtherPage({super.key});

  @override
  Widget build(BuildContext context, OtherNotifier notifier) {
    return Scaffold(
      appBar: AppBar(
        title: Text('txt_menu_other'.tr()),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                FormImagePicker(name: 'profile_pict'),
                const SizedBox(height: 32),
                Text(
                  notifier.user?.name ?? '',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
              ],
            ),
            Column(
              children: [
                ElevatedButton(
                  onPressed: () {
                    LocaleHelper().showLocaleDialog(context);
                  },
                  child: Text('txt_button_change_locale'.tr()),
                ),
                ElevatedButton(
                  onPressed: () {
                    notifier.signOut();
                  },
                  child: Text('txt_button_logout'.tr()),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

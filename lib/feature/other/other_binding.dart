import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/route_binding.dart';
import 'package:suitcore_flutter_base_code_provider/feature/other/other_notifier.dart';

class OtherBinding extends Bindings {
  @override
  List<SingleChildWidget> dependencies(dynamic args) {
    return [
      ChangeNotifierProvider(create: (_) => OtherNotifier()),
    ];
  }
}
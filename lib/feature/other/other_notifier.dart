import 'package:flutter/material.dart';
import 'package:suitcore_flutter_base_code_provider/feature/auth/auth_controller.dart';
import 'package:suitcore_flutter_base_code_provider/model/user.dart';

class OtherNotifier extends ChangeNotifier {
  final AuthController authController = AuthController.find;

  User? get user => authController.user;

  Future<void> signOut() async {
    await authController.signOut();
  }
}

import 'package:flutter/material.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/main_navigation.dart';
import 'package:suitcore_flutter_base_code_provider/feature/places/place_detail/place_detail_page.dart';
import 'package:suitcore_flutter_base_code_provider/model/place.dart';

class PlaceListItem extends StatelessWidget {
  final int index;
  final Place mData;
  const PlaceListItem({super.key, required this.index, required this.mData});
  final image = "https://picsum.photos/200/300";

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3.0,
      margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      child: InkWell(
        splashColor: Colors.orangeAccent,
        onTap: () {
          Navigation.instance.push(context, PlaceDetailPage.route, arguments: mData.id.toString());
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 16),
          child: ListTile(
            leading: CircleAvatar(
              backgroundImage: NetworkImage(image),
              radius: 30,
            ),
            title: Text(mData.name ?? ''),
            subtitle: Padding(
              padding: const EdgeInsets.only(top: 4),
              child: Text(
                mData.description ?? '',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(fontSize: 12),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:suitcore_flutter_base_code_provider/model/place.dart';

import 'place_item.dart';

class PlacesShimmer extends StatelessWidget {
  PlacesShimmer({Key? key}) : super(key: key);

  static final beginColor = Colors.grey.withOpacity(0.05);
  static const endColor = Colors.white;

  final Gradient gradient = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.centerRight,
    colors: [beginColor, beginColor, endColor, beginColor, beginColor],
    stops: const [0.0, 0.35, 0.5, 0.65, 1.0],
  );

  @override
  Widget build(BuildContext context) {
    return Shimmer(
      gradient: gradient,
      child: _buildPlaceListShimmer(),
    );
  }
}

List<Place> _dummyPlace() {
  List<Place> data = [];

  for (int i = 0; i < 9; i++) {
    var usr = Place(id: i, name: "name", description: "email");
    data.add(usr);
  }
  return data;
}

ListView _buildPlaceListShimmer() {
  return ListView.builder(
    itemCount: _dummyPlace().length,
    itemBuilder: (context, index) {
      return PlaceListItem(
        index: index,
        mData: _dummyPlace()[index],
      );
    },
  );
}

import 'package:suitcore_flutter_base_code_provider/data/remote/base/route_page.dart';
import 'places_binding.dart';
import 'places_page.dart';

final placesRoute = [
  RoutePage(
    name: PlacesPage.route,
    page: (args) => const PlacesPage(),
    binding: PlacesBinding(),
  ),
];

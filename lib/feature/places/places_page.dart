import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh_flutter3/pull_to_refresh_flutter3.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/notifier_view.dart';
import 'package:suitcore_flutter_base_code_provider/feature/places/widgets/place_item.dart';
import 'package:suitcore_flutter_base_code_provider/feature/places/widgets/places_shimmer.dart';
import 'package:suitcore_flutter_base_code_provider/utills/widget/state_handle_widget.dart';
import 'package:suitcore_flutter_base_code_provider/utills/widget/sm_app_bar.dart';
import 'places_notifier.dart';

class PlacesPage extends NotifierView<PlacesNotifier> {
  static const String route = '/places';

  const PlacesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, PlacesNotifier notifier) {
    return Scaffold(
      appBar: SMAppBar.primaryAppbar(titleString: 'txt_menu_home'.tr()),
      body: Consumable(
        () => StateHandleWidget(
          loadingEnabled: notifier.isShimmering,
          loadingView: PlacesShimmer(),
          emptyEnabled: notifier.isEmptyData,
          errorEnabled: notifier.isError,
          errorMessageDialog: notifier.message,
          errorTitle: 'txt_error_general'.tr(),
          onRetryPressed: notifier.refreshPage,
          body: SmartRefresher(
            enablePullDown: true,
            enablePullUp: notifier.hasNext,
            controller: notifier.refreshController,
            onRefresh: notifier.refreshPage,
            onLoading: notifier.loadNextPage,
            child: ListView.builder(
              itemCount: notifier.dataList.length,
              itemBuilder: (context, index) {
                final item = notifier.dataList[index];
                return PlaceListItem(
                  index: index,
                  mData: item,
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

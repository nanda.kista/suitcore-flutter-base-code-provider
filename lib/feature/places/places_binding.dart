import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/route_binding.dart';

import 'places_notifier.dart';

class PlacesBinding extends Bindings {
  @override
  List<SingleChildWidget> dependencies(args) {
    return [
      ChangeNotifierProvider(
        create: (_) => PlacesNotifier()..onInit(args),
      ),
    ];
  }
}

import 'package:flutter/material.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/api_services.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/base_notifier.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/errorhandler/error_handler.dart';
import 'package:suitcore_flutter_base_code_provider/model/place.dart';

class PlaceDetailNotifier extends BaseNotifier<Place> {
  String tag = 'PlaceDetailController::-> ';

  late String id;

  @override
  Future<void> onInit([dynamic args]) async {
    super.onInit();

    id = args as String;

    // load cache then fetch data.
    getCache(id: id).then((value) => refreshPage());

    // only fetch data.
    // refreshPage();
  }

  @override
  String get cacheKey => "places/$id";

  @override
  get statusData => dataObj;

  @override
  void refreshPage([BuildContext? context]) async {
    await onGetPlaceDetail();
  }

  Future<void> onGetPlaceDetail() async {
    loadingState();

    try {
      final client = await createClient();

      final result = await client
          .getPlaceDetail(cancelToken, int.parse(id))
          .validateResponse()
          .safeError();

      await saveCacheAndFinish(data: result.data);
    } catch (error) {
      finishLoadData(errorMessage: error.toString());
    }
  }
}

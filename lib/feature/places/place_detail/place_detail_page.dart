import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:pull_to_refresh_flutter3/pull_to_refresh_flutter3.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/notifier_view.dart';
import 'package:suitcore_flutter_base_code_provider/feature/places/place_detail/widgets/place_detail_content.dart';
import 'package:suitcore_flutter_base_code_provider/resources/resources.dart';
import 'package:suitcore_flutter_base_code_provider/utills/widget/loading_overlay.dart';
import 'package:suitcore_flutter_base_code_provider/utills/widget/state_handle_widget.dart';
import 'package:suitcore_flutter_base_code_provider/utills/widget/sm_app_bar.dart';
import 'place_detail_notifier.dart';

class PlaceDetailPage extends NotifierView<PlaceDetailNotifier> {
  static const String route = '/place/detail';

  const PlaceDetailPage({super.key});

  @override
  Widget build(BuildContext context, PlaceDetailNotifier notifier) {
    const imageStatic = "https://picsum.photos/200/300";

    return SMAppBar(
      title: 'txt_menu_detail'.tr(),
      backgroundColor: AppColors.colorSecondary,
      body: Consumable(
        () => StateHandleWidget(
          loadingView: const LoadingOverlay(),
          loadingEnabled: notifier.isShimmering,
          onRetryPressed: notifier.refreshPage,
          errorEnabled: notifier.isError,
          errorTitle: 'txt_error_general'.tr(),
          emptyEnabled: notifier.isEmptyData,
          emptySubtitle: 'txt_empty_place'.tr(),
          errorMessageDialog: notifier.message,
          body: SmartRefresher(
            enablePullDown: true,
            enablePullUp: notifier.hasNext,
            controller: notifier.refreshController,
            onRefresh: notifier.refreshPage,
            onLoading: notifier.loadNextPage,
            child: PlaceDetailContent(
              imageStatic: imageStatic,
              place: notifier.dataObj,
            ),
          ),
        ),
      ),
    );
  }
}

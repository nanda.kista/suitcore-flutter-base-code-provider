import 'package:suitcore_flutter_base_code_provider/data/remote/base/route_page.dart';
import 'place_detail_binding.dart';
import 'place_detail_page.dart';

final placeDetailRoute = [
  RoutePage(
    name: PlaceDetailPage.route,
    page: (args) => const PlaceDetailPage(),
    binding: PlaceDetailBinding(),
  ),
];

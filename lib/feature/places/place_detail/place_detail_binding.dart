import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/route_binding.dart';

import 'place_detail_notifier.dart';

class PlaceDetailBinding extends Bindings {
  @override
  List<SingleChildWidget> dependencies(dynamic args) {
    return [
      ChangeNotifierProvider(
        create: (_) => PlaceDetailNotifier()..onInit(args),
      ),
    ];
  }
}

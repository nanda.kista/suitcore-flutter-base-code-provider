import 'package:flutter/cupertino.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/api_services.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/base_notifier.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/errorhandler/error_handler.dart';
import 'package:suitcore_flutter_base_code_provider/model/place.dart';

class PlacesNotifier extends BaseNotifier<Place> {
  String tag = 'PlacesController::-> ';

  @override
  void onInit([dynamic args]) {
    super.onInit();

    // load cache then fetch data.
    getCache().then((value) => refreshPage());

    // only fetch data.
    // refreshPage();
  }

  @override
  String get cacheKey => "places";

  @override
  get statusData => dataList;

  @override
  void loadNextPage([BuildContext? context]) {
    loadDataList(page: page + 1);
  }

  @override
  void refreshPage([BuildContext? context]) async {
    await loadDataList();
  }

  Future<void> loadDataList({int page = 1}) async {

    try {
      loadingState();
      final client = await createClient();
      final result = await client
          .getPlaces(cancelToken, page, perPage)
          .validateResponse()
          .safeError();

      if (page == 1) {
        await saveCacheAndFinish(page: page, list: result.data);
      } else {
        finishLoadData(page: page, list: result.data);
      }
    } catch (error) {
      finishLoadData(errorMessage: error.toString());
    }
  }
}

// import 'package:get/get.dart';
import 'dart:async';

import 'package:hive/hive.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/main_navigation.dart';
import 'package:suitcore_flutter_base_code_provider/initializer.dart';
import 'package:suitcore_flutter_base_code_provider/model/storage.dart';
import 'package:suitcore_flutter_base_code_provider/model/user.dart';
import 'package:suitcore_flutter_base_code_provider/data/local/secure/secure_storage_manager.dart';
import 'package:suitcore_flutter_base_code_provider/feature/dashboardtab/dashboard_tab_page.dart';
import 'package:suitcore_flutter_base_code_provider/feature/loader/loading_page.dart';
import 'package:suitcore_flutter_base_code_provider/feature/login/login_page.dart';
import 'package:suitcore_flutter_base_code_provider/utills/helper/map_helper.dart';

import 'auth_state.dart';

class AuthController {
  static AuthController get find => sl<AuthController>();

  AppType authState = AppType.INITIAL;
  AppType? get state => authState;

  StreamController<AppType> authStreamController = StreamController<AppType>();
  Stream<AppType?> get stream => authStreamController.stream;

  var storage = Hive.box<Storage>((Storage).toString());
  var secureStorage = SecureStorageManager();

  User? get user {
    final user = storage.get((User).toString());
    if (user != null) {
      return User.fromJson(convertDynamicMap(user.value));
    } else {
      return null;
    }
  }

  AuthController() {
    authStreamController.add(authState);
    authStreamController.stream.distinct().listen((state) {
      authChanged(state);
    });
  }

  Future<void> authChanged(AppType state) async {
    if (state == AppType.INITIAL) {
      await setup();
      await checkToken();
    } else if (state == AppType.UNAUTHENTICATED) {
      await clearData();
      Navigation.instance.pushAllReplacementNoContext(LoginPage.route);
    } else if (state == AppType.AUTHENTICATED) {
      Navigation.instance.pushAllReplacementNoContext((DashboardTabPage.route));
    } else {
      Navigation.instance.pushAllReplacementNoContext((LoadingPage.route));
    }
  }

  Future<void> checkToken() async {
    if (storage.get((User).toString()) != null) {
      setAuth();
    } else {
      signOut();
    }
  }

  Future<void> clearData() async {
    storage.clear();
    await secureStorage.setToken(value: '');
  }

  Future<void> saveAuthData({required User user, required String token}) async {
    storage.put((User).toString(),
        Storage(key: (User).toString(), value: user.toJson()));
    await secureStorage.setToken(value: token);
    setAuth();
  }

  Future<void> signOut() async {
    await secureStorage.setToken(value: '');
    clearData();
    authState= AppType.UNAUTHENTICATED;
    authStreamController.add(authState);
  }

  void setAuth() {
    authState= AppType.AUTHENTICATED;
    authStreamController.add(authState);
  }

  Future<void> setup() async {
    if (storage.isNotEmpty) {
      final currentTime = DateTime.now();
      final keysToDelete = storage.keys.where((key) {
        final storageItem = storage.get(key) as Storage;
        return (key != (User).toString()) &&
            (storageItem.expiredDate.isBefore(currentTime) ||
                storageItem.expiredDate.isAtSameMomentAs(currentTime));
      }).toList();

      await Future.wait(keysToDelete.map((key) => storage.delete(key)));
    }
  }
}

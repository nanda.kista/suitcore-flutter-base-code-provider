import 'package:flutter/material.dart';
import 'package:suitcore_flutter_base_code_provider/resources/resources.dart';

class LoadingPage extends StatelessWidget {
  static const String route = '/loader';

  const LoadingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: CircularProgressIndicator(
          color: AppColors.colorPrimary,
        ),
      ),
    );
  }
}

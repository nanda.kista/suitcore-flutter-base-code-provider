import 'package:suitcore_flutter_base_code_provider/feature/loader/loading_page.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/route_page.dart';

final loaderRoute = [
  RoutePage(
    name: LoadingPage.route,
    page: (args) => const LoadingPage(),
  ),
];

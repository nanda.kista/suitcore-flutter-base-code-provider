import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:suitcore_flutter_base_code_provider/resources/resources.dart';
import 'package:suitcore_flutter_base_code_provider/utills/widget/base_empty_page.dart';

class DialogPage extends StatelessWidget {
  const DialogPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('txt_menu_dialog'.tr()),
      ),
      body: EmptyPage(
        image: AppImages.emptyStateCode.image().image,
        message: "No dialog added yet",
        buttonText: "Refresh",
        onPressed: () {},
      ),
    );
  }
}

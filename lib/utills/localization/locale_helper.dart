import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:sizer/sizer.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/main_navigation.dart';
import 'package:suitcore_flutter_base_code_provider/initializer.dart';
import 'package:suitcore_flutter_base_code_provider/model/storage.dart';
import 'package:suitcore_flutter_base_code_provider/resources/resources.dart';

class LocaleHelper {
  static LocaleHelper get instance => sl<LocaleHelper>();

  final Map<String, Locale> locales = {
    'English': const Locale('en'),
    'Indonesia': const Locale('id'),
  };

  final fallbackLocale = const Locale('en');

  Future<T?> showLocaleDialog<T>(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Choose Language"),
          content: SizedBox(
            width: double.maxFinite,
            child: ListView.separated(
              shrinkWrap: true,
              itemBuilder: (context, index) => InkWell(
                onTap: () {
                  final locale = locales.entries.toList()[index].value;
                  final localeName = locales.entries.toList()[index].key;
                  updateLocale(
                    context,
                    locale,
                    localeName,
                  );

                  Navigation.instance.pop(context);
                },
                child: Padding(
                  padding: EdgeInsets.all(10.sp),
                  child: Text(locales.entries.toList()[index].key),
                ),
              ),
              separatorBuilder: (context, index) => const Divider(
                color: AppColors.black,
              ),
              itemCount: locales.length,
            ),
          ),
        );
      },
    );
  }

  Future<void> updateLocale(BuildContext context, Locale locale, String name) async {
    await saveLanguagesToCache(name);
    if (context.mounted) context.setLocale(locale);
    await WidgetsBinding.instance.performReassemble();
  }

  Future<void> saveLanguagesToCache(String name) {
    if (name == "English") {
      return Hive.box<Storage>((Storage).toString()).put(
          (Locale).toString(), Storage(key: (Locale).toString(), value: "en"));
    } else {
      return Hive.box<Storage>((Storage).toString()).put(
          (Locale).toString(), Storage(key: (Locale).toString(), value: "id"));
    }
  }

  Locale get getCurrentLocale {
    String? currentLocale = Hive.box<Storage>((Storage).toString())
            .get((Locale).toString())
            ?.value
            .toString() ??
        "en";
    if (currentLocale == "en") {
      return const Locale('en');
    } else {
      return const Locale('id');
    }
  }
}

import 'package:flutter/material.dart';
import 'package:suitcore_flutter_base_code_provider/utills/widget/image_load.dart';
import 'package:suitcore_flutter_base_code_provider/utills/widget/sm_app_bar.dart';

class ImagePreviewPage extends StatelessWidget {
  final String src;
  final bool isAsset;
  final String? title;
  final TextStyle? titleStyle;

  const ImagePreviewPage({
    Key? key,
    required this.src,
    this.isAsset = true,
    this.title,
    this.titleStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SMAppBar.primaryAppbar(
        titleString: title ?? '',
        titleStyle: titleStyle,
      ),
      body: Center(
        child: ImageLoad(
          src: src,
          isAsset: isAsset,
        ),
      ),
    );
  }
}

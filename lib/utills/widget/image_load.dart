import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:suitcore_flutter_base_code_provider/resources/resources.dart';
import 'package:suitcore_flutter_base_code_provider/utills/helper/image_helper.dart';
import 'package:suitcore_flutter_base_code_provider/utills/widget/loading_overlay.dart';
import 'package:suitcore_flutter_base_code_provider/utills/extension/string_extensions.dart';
import 'media/image_preview_page.dart';

enum ShapeImage { oval, react, circle }

class ImageLoad extends StatelessWidget {
  final String? src;
  final double? width;
  final double? height;
  final VoidCallback? onTap;
  final BorderRadiusGeometry? borderRadius;
  final BoxFit fit;
  final Color? color;
  final Widget? errorWidget;
  final Widget? loadingWidget;
  final Alignment alignment;
  final ShapeImage shapeImage;

  /// Called this placeholder src when [src] is null or empty
  /// If you fill the [generateByName], this placeholderSrc will be replace
  /// or not work
  final String? placeholderSrc;

  /// BoxFit for placeholder
  final BoxFit? placeholderFit;

  /// Custom widget that called when [src] is null or empty
  final Widget? placeholderWidget;

  /// If true -> enable on tap to redirect MediaPreview Page
  final bool enablePreview;
  final String? previewTitle;
  final TextStyle? previewTitleStyle;

  /// Must be set to true manually if you need load from asset
  /// but the source (src) is not from
  ///
  /// **(lib/resources/.. )** or **(assets/.. )**
  final bool isAsset;

  /// Fill this to generated network image source by given Name.
  /// If you fill this field and [placeholderSrc], the placeholderSrc will not work
  final String? generateByName;

  const ImageLoad({
    Key? key,
    this.src,
    this.width,
    this.height,
    this.onTap,
    this.borderRadius,
    this.fit = BoxFit.cover,
    this.enablePreview = false,
    this.placeholderFit,
    this.placeholderWidget,
    this.placeholderSrc,
    this.isAsset = false,
    this.color,
    this.previewTitle,
    this.previewTitleStyle,
    this.errorWidget,
    this.loadingWidget,
    this.shapeImage = ShapeImage.react,
    this.alignment = Alignment.center,
    this.generateByName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (src.isNotNullAndNotEmpty) {
      return BaseImage(
        src: src.toString(),
        width: width,
        height: height,
        fit: fit,
        borderRadius: borderRadius,
        enablePreview: enablePreview,
        onTapImage: onTap,
        isAsset: isAsset,
        color: color,
        previewTitle: previewTitle,
        previewTitleStyle: previewTitleStyle,
        errorWidget: errorWidget,
        loadingWidget: loadingWidget,
        shapeImage: shapeImage,
        alignment: alignment,
      );
    } else {
      return placeholderWidget ??
          BaseImage(
            src: generateByName.isNotNullAndNotEmpty
                ? ImageHelper.generateAvatarByName(
                    generateByName ?? 'user',
                  )
                : placeholderSrc ?? AppImages.icLogoSuitcoreMain.path,
            width: width,
            height: height,
            fit: placeholderFit ?? BoxFit.contain,
            borderRadius: borderRadius,
            enablePreview: enablePreview,
            onTapImage: onTap,
            isAsset: isAsset,
            color: color,
            previewTitle: previewTitle,
            previewTitleStyle: previewTitleStyle,
            shapeImage: shapeImage,
            alignment: alignment,
          );
    }
  }
}

class BaseImage extends StatelessWidget {
  final String src;
  final double? width;
  final double? height;
  final VoidCallback? onTapImage;
  final BorderRadiusGeometry? borderRadius;
  final BoxFit fit;
  final bool enablePreview;
  final bool isAsset;
  final Color? color;
  final String? previewTitle;
  final TextStyle? previewTitleStyle;
  final Widget? errorWidget;
  final Widget? loadingWidget;
  final ShapeImage shapeImage;
  final Alignment alignment;

  const BaseImage({
    Key? key,
    required this.src,
    this.width,
    this.height,
    this.onTapImage,
    this.borderRadius,
    this.fit = BoxFit.fill,
    this.enablePreview = false,
    this.isAsset = false,
    this.color,
    this.previewTitle,
    this.previewTitleStyle,
    this.errorWidget,
    this.loadingWidget,
    this.shapeImage = ShapeImage.react,
    this.alignment = Alignment.center,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (shapeImage == ShapeImage.circle) {
      assert(
        height != null && width != null,
        "Height and Width cannot be null if shapeImage is ShapeImage.circle, ",
      );
    }

    return GestureDetector(
      onTap: enablePreview
          ? () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ImagePreviewPage(
                    src: src,
                    isAsset: isAsset,
                    title: previewTitle,
                    titleStyle: previewTitleStyle,
                  ),
                ),
              )
          : onTapImage,
      child: _determineShapeImage(),
    );
  }

  Widget _determineShapeImage() {
    switch (shapeImage) {
      case ShapeImage.circle:
        return ClipRRect(
          borderRadius: BorderRadius.circular(height! / 2),
          child: _determineImageWidget(),
        );
      case ShapeImage.oval:
        return ClipOval(child: _determineImageWidget());
      case ShapeImage.react:
        return ClipRRect(
          borderRadius: borderRadius ?? BorderRadius.circular(0),
          child: _determineImageWidget(),
        );
      default:
        return _determineImageWidget();
    }
  }

  Widget _determineImageWidget() {
    final isFromRemote = src.startsWith('http');
    final isSvg = src.endsWith('svg');
    final isAssets = isAsset ||
        src.startsWith('lib/resources/') ||
        src.startsWith('assets/');

    if (isSvg) {
      return SvgPicture.asset(
        src,
        width: width,
        height: height,
        colorFilter:
            color != null ? ColorFilter.mode(color!, BlendMode.srcIn) : null,
        fit: fit,
        alignment: alignment,
      );
    } else if (isFromRemote) {
      return CachedNetworkImage(
        imageUrl: src,
        color: color,
        fit: fit,
        width: width,
        height: height,
        alignment: alignment,
        placeholder: (context, url) => SizedBox(
          height: height,
          width: width,
          child: loadingWidget ?? const LoadingOverlay(),
        ),
        errorWidget: (context, url, error) {
          debugPrint('Error load network image $url $error');
          return SizedBox(
            height: height,
            width: width,
            child: errorWidget ?? const Icon(Icons.error),
          );
        },
      );
    } else if (isAssets) {
      return Image.asset(
        src,
        width: width,
        height: height,
        fit: fit,
        color: color,
        alignment: alignment,
        errorBuilder: (context, error, stackTrace) {
          debugPrint('Error load asset image $error');
          return SizedBox(
            height: height,
            width: width,
            child: errorWidget ?? const Icon(Icons.error),
          );
        },
      );
    } else {
      return Image.file(
        File(src),
        width: width,
        height: height,
        fit: fit,
        color: color,
        alignment: alignment,
        errorBuilder: (context, error, stackTrace) {
          debugPrint('Error load file image $error');
          return SizedBox(
            height: height,
            width: width,
            child: errorWidget ?? const Icon(Icons.error),
          );
        },
      );
    }
  }
}

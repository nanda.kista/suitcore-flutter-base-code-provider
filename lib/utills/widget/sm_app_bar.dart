import 'package:flutter/material.dart';
import 'package:suitcore_flutter_base_code_provider/resources/resources.dart';
import 'package:suitcore_flutter_base_code_provider/utills/widget/colored_status_bar.dart';

class SMAppBar extends StatelessWidget {
  const SMAppBar(
      {Key? key,
      this.color,
      required this.body,
      this.brightness = Brightness.dark,
      this.backImage,
      this.title,
      this.titleStyle,
      this.backgroundColor,
      this.bgScaffold,
      this.actions,
      this.backCallback,
      this.resizeToAvoidBottomInset,
      this.usingDefaultColorStatusBar = false,
      this.bottom})
      : super(key: key);

  final Color? color;
  final Widget body;
  final Brightness brightness;
  final ImageProvider? backImage;
  final String? title;
  final TextStyle? titleStyle;
  final Color? backgroundColor;
  final Color? bgScaffold;
  final List<Widget>? actions;
  final void Function()? backCallback;
  final bool usingDefaultColorStatusBar;
  final bool? resizeToAvoidBottomInset;
  final PreferredSizeWidget? bottom;

  @override
  Widget build(BuildContext context) {
    return ColoredStatusBar(
      brightness: brightness,
      color: usingDefaultColorStatusBar ? AppColors.colorPrimary : color,
      child: Scaffold(
        backgroundColor: bgScaffold,
        resizeToAvoidBottomInset: resizeToAvoidBottomInset,
        appBar: AppBar(
          title: Text(
            title ?? '',
            style: titleStyle ??
                Theme.of(context).textTheme.displaySmall?.copyWith(
                    fontWeight: FontWeight.w500, color: Colors.white),
          ),
          centerTitle: false,
          backgroundColor: backgroundColor ?? Colors.transparent,
          elevation: 0.0,
          leading: InkWell(
            onTap: backCallback ?? () => Navigator.of(context).pop(),
            child: backImage == null
                ? const Icon(
                    Icons.arrow_back_ios_rounded,
                    color: Colors.white,
                  )
                : Image(
                    image: backImage!,
                    width: 24,
                    height: 24,
                  ),
          ),
          actions: actions ?? [],
          bottom: bottom,
        ),
        body: body,
      ),
    );
  }

  /// Gunakan `Primary Appbar` untuk membuat AppBar default secara global
  /// agar jenis AppBar utama terpusat dan modular
  /// *boleh di-edit sesuai kebutuhan*
  ///
  static AppBar primaryAppbar({
    required String titleString,
    TextStyle? titleStyle,
  }) {
    return AppBar(
      backgroundColor: AppColors.colorPrimary,
      title: Text(
        titleString,
        style: titleStyle ?? const TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  /// `Secondary Appbar` digunakan apabila membutuhkan jenis AppBar yang berbeda
  /// dari AppBar utama. Misal untuk AppBar Detail
  /// *boleh di-edit sesuai kebutuhan*
  ///
  static AppBar secondaryAppbar({required String titleString}) {
    return AppBar(
      iconTheme: const IconThemeData(
        color: Colors.white,
      ),
      backgroundColor: AppColors.colorSecondary,
      title: Text(
        titleString,
        style: const TextStyle(
          color: Colors.white,
        ),
      ),
      actions: [
        IconButton(
          splashColor: AppColors.colorPrimary,
          icon: const Icon(Icons.edit),
          onPressed: () {
            //
          },
        ),
      ],
    );
  }

  /// Apabila ada tambahan jenis AppBar, silahkan untuk ditambahkan
  /// sesuai kebutuhan
  ///
}

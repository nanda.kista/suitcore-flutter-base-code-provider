extension StringNullExtension on String? {
  bool get isNotNullAndNotEmpty {
    return this != null && this != '' && this != 'null';
  }
}
import 'package:flutter/material.dart';
import 'package:suitcore_flutter_base_code_provider/data/remote/base/main_navigation.dart';
import 'package:suitcore_flutter_base_code_provider/utills/widget/loading_overlay.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class DialogHelper {
  static loading(BuildContext context, {bool barrierDismissible = false}) {
    return showGeneralDialog(
      context: context,
      barrierLabel: 'Barrier',
      barrierDismissible: barrierDismissible,
      barrierColor: Colors.black.withOpacity(0.5),
      pageBuilder: (_, __, ___) {
        return WillPopScope(
          onWillPop: () async => barrierDismissible,
          child: Center(
            child: Container(
              height: 80,
              width: 80,
              decoration: BoxDecoration(
                color: Theme.of(context).scaffoldBackgroundColor,
                borderRadius: BorderRadius.circular(12),
              ),
              padding: const EdgeInsets.all(16),
              child: const LoadingOverlay(),
            ),
          ),
        );
      },
    );
  }

  static dismiss(BuildContext context) => Navigation.instance.pop(context);

  static alertError(
    BuildContext context, {
    String? title,
    required String message,
  }) {
    showTopSnackBar(
      Overlay.of(context),
      CustomSnackBar.error(
        message: message,
        backgroundColor: Colors.red,
        textAlign: TextAlign.left,
        maxLines: 3,
        textStyle: const TextStyle(color: Colors.white, fontSize: 14),
      ),
      displayDuration: const Duration(milliseconds: 1000),
    );
  }

  static alertSuccess(BuildContext context, {required String message}) {
    showTopSnackBar(
      Overlay.of(context),
      CustomSnackBar.success(
        message: message,
        backgroundColor: Colors.green,
        textAlign: TextAlign.left,
        maxLines: 3,
        textStyle: const TextStyle(color: Colors.white, fontSize: 14),
      ),
      displayDuration: const Duration(milliseconds: 1000),
    );
  }

  static alertWarning(
      BuildContext context, {
        String? title,
        required String message,
      }) {
    showTopSnackBar(
      Overlay.of(context),
      CustomSnackBar.error(
        message: message,
        backgroundColor: Colors.orange,
        textAlign: TextAlign.left,
        maxLines: 3,
        textStyle: const TextStyle(color: Colors.white, fontSize: 14),
      ),
      displayDuration: const Duration(milliseconds: 1000),
    );
  }

  static alertInfo(BuildContext context, {required String message}) {
    showTopSnackBar(
      Overlay.of(context),
      CustomSnackBar.info(
        message: message,
        textAlign: TextAlign.left,
        maxLines: 3,
        textStyle: const TextStyle(color: Colors.white, fontSize: 14),
      ),
      displayDuration: const Duration(milliseconds: 1000),
    );
  }
}

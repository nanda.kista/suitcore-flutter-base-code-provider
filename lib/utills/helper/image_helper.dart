class ImageHelper {
  static String generateAvatarByName(String name) {
    return 'https://ui-avatars.com/api/?size=256&name=$name';
  }
}
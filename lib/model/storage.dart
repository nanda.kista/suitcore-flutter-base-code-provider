// ignore_for_file: type=lint

import 'package:hive/hive.dart';
import 'package:suitcore_flutter_base_code_provider/data/local/hive/hive_types.dart';
import 'package:suitcore_flutter_base_code_provider/data/local/hive/hive_adapters.dart';
import 'package:suitcore_flutter_base_code_provider/data/local/hive/fields/storage_fields.dart';

part 'storage.g.dart';

@HiveType(typeId: HiveTypes.storage, adapterName: HiveAdapters.storage)
class Storage extends HiveObject {
  Storage({required this.key, required this.value, DateTime? expiredDate})
      : expiredDate =
            expiredDate ?? DateTime.now().add(const Duration(days: 10));

  @HiveField(StorageFields.key)
  final String key;
  @HiveField(StorageFields.value)
  final dynamic value;
  @HiveField(StorageFields.expiredDate)
  final DateTime expiredDate;

  factory Storage.fromJson(Map<String, dynamic> json) => Storage(
        key: json["key"],
        value: json["value"],
        expiredDate: json["expiredDate"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
        "expiredDate": expiredDate,
      };
}

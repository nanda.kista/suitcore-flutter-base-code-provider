import 'package:flutter/material.dart';
import 'package:suitcore_flutter_base_code_provider/resources/resources.dart';

class AppTheme {
  static ThemeData buildThemeData(bool darkMode) {
    return ThemeData(
      useMaterial3: false,
      primaryColor: AppColors.colorPrimary,
      brightness: (darkMode) ? Brightness.light : Brightness.light,
      scaffoldBackgroundColor: (darkMode) ? AppColors.black : AppColors.white,
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      appBarTheme: (darkMode) ? darkAppBar() : lightAppBar(),
      fontFamily: 'Lato',
      floatingActionButtonTheme: const FloatingActionButtonThemeData(),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          backgroundColor: AppColors.colorPrimary,
        ),
      ),
      bottomNavigationBarTheme:
          (darkMode) ? darkNavigation() : lightNavigation(),
      textTheme: const TextTheme(
        displayLarge: TextStyle(
          fontSize: 24, // Navbar
          fontWeight: FontWeight.w700,
          color: Colors.white,
        ),
        displayMedium: TextStyle(
          fontSize: 20, // Banner
          fontWeight: FontWeight.bold,
          color: Colors.white,
        ),
        displaySmall: TextStyle(
          fontSize: 18, // Normal
          fontWeight: FontWeight.w600,
          color: Colors.black,
        ),
        headlineMedium: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.normal,
            color: Colors.black),
        headlineSmall: TextStyle(
          fontSize: 14, // SubNormal
          fontWeight: FontWeight.w500,
          color: Colors.black54,
        ),
        titleLarge: TextStyle(
          fontSize: 14,
          color: AppColors.colorPrimary,
          fontWeight: FontWeight.w500,
        ),
        bodyLarge: TextStyle(fontSize: 16, color: Colors.black87),
        bodyMedium: TextStyle(fontSize: 14, color: Colors.black87),
        bodySmall: TextStyle(fontSize: 12, color: Colors.black87),
        labelLarge: TextStyle(fontSize: 16, color: Colors.white),
      ),
      inputDecorationTheme: inputDecoration(darkMode), colorScheme: ThemeData().colorScheme.copyWith(
            primary: AppColors.colorPrimary,
            secondary: AppColors.colorSecondary,
          ).copyWith(background: (darkMode) ? AppColors.black : AppColors.white),
    );
  }

  static AppBarTheme lightAppBar() {
    return const AppBarTheme(
      iconTheme: IconThemeData(
        color: Colors.black, //change your color here
      ),
      color: Colors.white,
      elevation: 0,
    );
  }

  static BottomNavigationBarThemeData lightNavigation() {
    return const BottomNavigationBarThemeData(
      backgroundColor: Colors.white,
      unselectedItemColor: Colors.black,
      selectedItemColor: Colors.deepOrangeAccent,
      elevation: 0,
    );
  }

  static AppBarTheme darkAppBar() {
    return const AppBarTheme(
      iconTheme: IconThemeData(
        color: Colors.white, //change your color here
      ),
      color: Colors.black,
      elevation: 0,
    );
  }

  static BottomNavigationBarThemeData darkNavigation() {
    return const BottomNavigationBarThemeData(
      backgroundColor: Colors.black,
      unselectedItemColor: Colors.white,
      selectedItemColor: Colors.deepOrangeAccent,
      elevation: 0,
    );
  }

  // Box Field
  static InputDecorationTheme inputDecoration(bool darkMode) {
    return InputDecorationTheme(
      filled: true,
      fillColor: Colors.grey[50],
      contentPadding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: const BorderSide(color: AppColors.borderColor, width: 1),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: const BorderSide(color: AppColors.borderColor, width: 1),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: BorderSide(color: AppColors.red.withOpacity(0.8), width: 1),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: const BorderSide(color: AppColors.red, width: 1.4),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: const BorderSide(color: AppColors.colorPrimary, width: 1),
      ),
      labelStyle: TextStyle(
        color: darkMode ? AppColors.white : AppColors.black,
      ),
      hintStyle: const TextStyle(color: AppColors.subHintColor, fontSize: 12),
    );
  }
}

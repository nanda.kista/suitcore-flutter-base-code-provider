# Suitcore Flutter Base Code GetX

Suitmedia Mobile's Flutter Boilerplate Project.

- GetX state management
- GetX List pull to refresh and infinite scroll

## Build

Open terminal di root project dan masukkan command dibawah.

Note: `<env>` adalah `staging` | `production`

### Android

    // Pilih salah satu

    // build apk
    sh build_apk.sh <env> 1.0.0-beta.1 1

    // build apk menggunakan version dari pubspec.yaml
    sh build_pub_apk.sh <env>

### iOS

    // Pilih salah satu

    // generate archive
    sh build_ios.sh <env> 1.0.0-beta.1 1

    // generate ipa adhoc
    sh build_ipa_adhoc.sh <env> 1.0.0-beta.1 1

    // build archive menggunakan version dari pubspec.yaml
    sh build_pub_ios.sh <env>

    // build ipa menggunakan version dari pubspec.yaml
    sh build_pub_ipa_adhoc.sh <env>

<br>

# Modul Generator

Project ini mendukung pembuatan modul dari template menggunakan `mason`. Untuk instalasi mason dapat dilihat disini [LINK](https://docs.google.com/document/d/12mn5gz0WYobQoIjYJCV6LZV7dlkQ3yUUj5W3-FGrgkM/edit?usp=sharing).

## Usage

Ada tiga jenis template mason yang disediakan:

1. `suit_list` : template untuk membuat screen berupa list page.

       mason make suit_list -c mason.json -o lib/feature

    Lalu akan muncul prompt input, contoh diisi `Page Name = news_list`

2. `suit_detail` : template untuk membuat screen berupa detail page.

       mason make suit_detail -c mason.json -o lib/feature

    Lalu akan muncul prompt input, contoh diisi `Page Name = news_detail`
       
3. `suit_page` : template untuk membuat screen empty page.

       mason make suit_page -c mason.json -o lib/feature

    Lalu akan muncul prompt input, contoh diisi `Page Name = custom_one`
       
Note: hasil generate masing-masing template bisa dikembangkan lagi dan boleh diubah sesuai kebutuhan.

## Route

Nama route dideklarasi di widget page.

    class LoginPage extends StatelessWidget {
      static const String route = '/login';
      ...
    }

Penggunaannya bisa seperti ini.

    Get.toNamed(LoginPage.route);

<br>

<hr>

References

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)
- [GetX: Reference](https://pub.dev/packages/get)
- [Mason CLI](https://github.com/felangel/mason)

<br><br>

# Fase Development

### Fase development
- Develop (Alpha)
- Internal test oleh analis (Beta)
- External test oleh klien (Beta)
- Production/Store

### Kondisi app belum rilis
- asumsikan target first release store adalah 1.0.0
- fase develop: 1.0.0-alpha.1
- internal testing: 1.0.0-beta.1
- analis report bug
- fase develop (bug fixing): 1.0.0-alpha.2
- internal testing: 1.0.0-beta.2
- external testing: 1.0.0-beta.2
- klien report bug
- fase develop (bug fixing): 1.0.0-alpha.3
- internal testing: 1.0.0-beta.3
- external testing: 1.0.0-beta.3
- internal dan external test ok
- up store 1.0.0

### Kondisi live app ada bug
- asumsikan yang sedang live di store adalah 1.0.0, maka target next release store untuk bug fix adalah 1.0.1
- fase develop: 1.0.1-alpha.1
- internal testing: 1.0.1-beta.1
- external testing: 1.0.1-beta.1
- internal dan external test ok
- up store 1.0.1

### Kondisi live app ada penambahan fitur baru
- asumsikan yang sedang live di store adalah 1.0.1, maka target next release store untuk fitur baru adalah 1.1.0
- fase develop: 1.1.0-alpha.1
- internal testing: 1.1.0-beta.1
- external testing: 1.1.0-beta.1
- internal dan external test ok
- up store 1.1.0

<br>

<hr>

Copyright ©2023 Suitmedia. All rights reserved.
